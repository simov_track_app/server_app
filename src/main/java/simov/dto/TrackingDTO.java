package simov.dto;

import simov.entity.AllowedZone;
import simov.entity.Tracking;

public class TrackingDTO {
    private long id;
    private String parentUsername;
    private String childUsername;
    private double lastLocationLatitude;
    private double lastLocationLongitude;
    private AllowedZone allowedZone;

    public TrackingDTO() {
    }

    public TrackingDTO(Tracking tracking) {
        this.id = tracking.getId();
        this.parentUsername = tracking.getParent().getName();
        this.childUsername = tracking.getChild().getName();
        this.lastLocationLatitude = tracking.getLastLocationLatitude();
        this.lastLocationLongitude = tracking.getLastLocationLongitude();
        this.allowedZone = tracking.getAllowedZone();

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getParentUsername() {
        return parentUsername;
    }

    public void setParentUsername(String parentUsername) {
        this.parentUsername = parentUsername;
    }

    public String getChildUsername() {
        return childUsername;
    }

    public void setChildUsername(String childUsername) {
        this.childUsername = childUsername;
    }

    public double getLastLocationLatitude() {
        return lastLocationLatitude;
    }

    public void setLastLocationLatitude(double lastLocationLatitude) {
        this.lastLocationLatitude = lastLocationLatitude;
    }

    public double getLastLocationLongitude() {
        return lastLocationLongitude;
    }

    public void setLastLocationLongitude(double lastLocationLongitude) {
        this.lastLocationLongitude = lastLocationLongitude;
    }

    public AllowedZone getAllowedZone() {
        return allowedZone;
    }

    public void setAllowedZone(AllowedZone allowedZone) {
        this.allowedZone = allowedZone;
    }
}
