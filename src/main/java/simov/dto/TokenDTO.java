package simov.dto;

import javax.validation.constraints.NotNull;

public class TokenDTO {
    @NotNull
    private String fcmToken;

    public TokenDTO() {
    }

    public TokenDTO(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
