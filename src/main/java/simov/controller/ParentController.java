package simov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import simov.dto.TokenDTO;
import simov.dto.TrackingDTO;
import simov.entity.AllowedZone;
import simov.entity.Child;
import simov.entity.Parent;
import simov.entity.Tracking;
import simov.exception.*;
import simov.repository.ChildRepository;
import simov.repository.ParentRepository;
import simov.repository.TrackingRepository;
import simov.service.NotificationService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class ParentController {
    private final ParentRepository parentRepository;

    private final ChildRepository childRepository;

    private final TrackingRepository trackingRepository;

    private final NotificationService notificationService;

    @Autowired
    public ParentController(ParentRepository parentRepository, ChildRepository childRepository, TrackingRepository trackingRepository, NotificationService notificationService) {
        this.parentRepository = parentRepository;
        this.childRepository = childRepository;
        this.trackingRepository = trackingRepository;
        this.notificationService = notificationService;
    }

    @PostMapping(value = "/parent", consumes = "application/json")
    public void createParent(@RequestBody Parent parent) {
        if (parent.getPassword() == null) {
            throw new InvalidInputException();
        }

        if (parentRepository.findByName(parent.getName()) != null) {
            throw new DuplicateParentException();
        }

        parentRepository.save(parent);
    }

    @PutMapping(value = "/parent/{parentUser}/token", consumes = "application/json")
    public void updateToken(@PathVariable String parentUser, @RequestBody TokenDTO token) {
        Parent parent = parentRepository.findByName(parentUser);
        if (parent == null) {
            throw new ParentNotFoundException();
        }

        parent.setFcmToken(token.getFcmToken());

        parentRepository.save(parent);
    }

    @RequestMapping(value = "/parent/{user}/childrenTracked", method = RequestMethod.GET, produces = "application/json")
    public Set<TrackingDTO> getChildrenBeingTracked(@PathVariable String user) {
        Parent parent = parentRepository.findByName(user);
        if (parent == null) {
            throw new ParentNotFoundException();
        }

        return parent.getTracked().stream()
                .map(TrackingDTO::new)
                .collect(Collectors.toSet());
    }

    @PostMapping(value = "/parent/{parentUsername}/children/{childUsername}/zone", consumes = "application/json")
    public void createAllowedZone(@PathVariable String parentUsername, @PathVariable String childUsername, @RequestBody AllowedZone allowedZone) throws IOException {
        Parent parent = parentRepository.findByName(parentUsername);
        Child child = childRepository.findByName(childUsername);
        if (parent == null) {
            throw new ParentNotFoundException();
        }

        if (child == null) {
            throw new ChildNotFoundException();
        }

        Tracking tracking = trackingRepository.findByParentAndChild(parent, child);
        if (tracking == null) {
            throw new TrackingNotFoundException();
        }

        tracking.setAllowedZone(allowedZone);
        trackingRepository.save(tracking);

        notificationService.postNotification(
                child.getFcmToken(),
                "New allowed zone",
                "Parent " + parent.getName() + " defined a new allowed zone.");

    }

    @PostMapping(value = "/parent/{parentUsername}/children/{childUsername}/last-location", consumes = "application/json")
    public void getChildLocation(@PathVariable String parentUsername, @PathVariable String childUsername) throws IOException {
        Parent parent = parentRepository.findByName(parentUsername);
        Child child = childRepository.findByName(childUsername);
        if (parent == null) {
            throw new ParentNotFoundException();
        }

        if (child == null) {
            throw new ChildNotFoundException();
        }

        Tracking tracking = trackingRepository.findByParentAndChild(parent, child);
        if (tracking == null) {
            throw new TrackingNotFoundException();
        }

        Map<String, String> data = new HashMap<>();
        data.put("last-location", "");
        data.put("trackingId", String.valueOf(tracking.getId()));
        notificationService.postData(
                child.getFcmToken(), data);
    }
}