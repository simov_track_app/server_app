package simov.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import simov.dto.LocationDTO;
import simov.dto.TokenDTO;
import simov.dto.TrackingDTO;
import simov.entity.Child;
import simov.entity.Parent;
import simov.entity.Tracking;
import simov.exception.*;
import simov.repository.ChildRepository;
import simov.repository.ParentRepository;
import simov.repository.TrackingRepository;
import simov.service.NotificationService;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class ChildController {
    private final ChildRepository childRepository;

    private final ParentRepository parentRepository;

    private final TrackingRepository trackingRepository;

    private final NotificationService notificationService;

    @Autowired
    public ChildController(ChildRepository childRepository, ParentRepository parentRepository, TrackingRepository trackingRepository, NotificationService notificationService) {
        this.childRepository = childRepository;
        this.parentRepository = parentRepository;
        this.trackingRepository = trackingRepository;
        this.notificationService = notificationService;
    }

    @PostMapping(value = "/children", consumes = "application/json")
    public void createChild(@RequestBody Child child) {
        if (childRepository.findByName(child.getName()) != null) {
            throw new DuplicateChildException();
        }

        childRepository.save(child);
    }

    @PostMapping(value = "/children/{childUser}/trackByParent", consumes = "application/json")
    public TrackingDTO addTrackingId(@PathVariable String childUser, @RequestBody Parent parentUsername) throws IOException {
        Child child = childRepository.findByName(childUser);
        if (child == null) {
            throw new ChildNotFoundException();
        }

        Parent parent = parentRepository.findByName(parentUsername.getName());
        if (parent == null) {
            throw new ParentNotFoundException();
        }

        if (trackingRepository.findByParentAndChild(parent, child) != null) {
            throw new DuplicateTrackingException();
        }

        Tracking tracking = new Tracking();
        tracking.setChild(child);
        tracking.setParent(parent);
        trackingRepository.save(tracking);

        notificationService.postNotification(
                parent.getFcmToken(),
                "New child is being tracked",
                child.getName() + " is now being tracked. You can now set the allowed zone.");

        return new TrackingDTO(tracking);
    }

    @GetMapping(value = "/children/{childUser}/trackByParent", produces = "application/json")
    public List<TrackingDTO> getTrackingRelationships(@PathVariable String childUser) {
        Child child = childRepository.findByName(childUser);
        if (child == null) {
            throw new ChildNotFoundException();
        }

        return child.getTracking().stream()
                .map(TrackingDTO::new)
                .collect(Collectors.toList());
    }

    @PutMapping(value = "/children/{childUser}/token", consumes = "application/json")
    public void setToken(@PathVariable String childUser, @RequestBody TokenDTO token) {
        Child child = childRepository.findByName(childUser);
        if (child == null) {
            throw new ChildNotFoundException();
        }

        child.setFcmToken(token.getFcmToken());

        childRepository.save(child);
    }

    @PostMapping(value = "/children/{childUser}/zoneTransition/{trackingId}")
    public void notifyChildTransitionedZone(@PathVariable String childUser,
                                            @PathVariable Long trackingId,
                                            @RequestParam(value = "out", defaultValue = "true") boolean out) throws IOException {
        Child child = childRepository.findByName(childUser);
        if (child == null) {
            throw new ChildNotFoundException();
        }
        Tracking tracking = trackingRepository.findOne(trackingId);
        if (tracking == null) {
            throw new TrackingNotFoundException();
        }

        notificationService.postNotification(
                tracking.getParent().getFcmToken(),
                out ? "Child out of zone!" : "Child entered Zone",
                child.getName() + " is " + (out ? "out of" : "in") + " its defined zone!");
    }

    @PostMapping(value = "/children/{childUser}/last-location/{trackingId}")
    public void notifyChildLastLocation(@PathVariable String childUser,
                                        @PathVariable Long trackingId,
                                        @RequestBody LocationDTO locationDTO) throws IOException {
        Child child = childRepository.findByName(childUser);
        if (child == null) {
            throw new ChildNotFoundException();
        }
        Tracking tracking = trackingRepository.findOne(trackingId);
        if (tracking == null) {
            throw new TrackingNotFoundException();
        }

        Map<String, String> data = new HashMap<>();
        data.put("lat", String.valueOf(locationDTO.getLocationLatitude()));
        data.put("long", String.valueOf(locationDTO.getLocationLongitude()));
        data.put("childName", child.getName());
        notificationService.postData(
                tracking.getParent().getFcmToken(),
                data);
    }

    @DeleteMapping(value = "/children/{childUser}/tracking/{trackingId}")
    public void deleteTracking(@PathVariable String childUser, @PathVariable Long trackingId) {
        Child child = childRepository.findByName(childUser);
        if (child == null) {
            throw new ChildNotFoundException();
        }
        Tracking tracking = trackingRepository.findOne(trackingId);
        if (tracking == null) {
            throw new TrackingNotFoundException();
        }

        trackingRepository.delete(tracking);
    }
}
