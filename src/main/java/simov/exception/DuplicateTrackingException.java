package simov.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Tracking already exists")
public class DuplicateTrackingException extends RuntimeException {
}
