package simov.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class Parent {
    @Id
    @NotNull
    @Size(min = 4)
    private String name;

    private String password;

    @Column(length = 400)
    private String fcmToken;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
    private Set<Tracking> tracked;

    public Parent() {
    }

    public Parent(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public Set<Tracking> getTracked() {
        return tracked;
    }

    public void setTracked(Set<Tracking> tracked) {
        this.tracked = tracked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}