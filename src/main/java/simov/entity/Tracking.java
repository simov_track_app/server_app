package simov.entity;

import javax.persistence.*;

@Entity
public class Tracking {
    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    private Parent parent;
    @ManyToOne
    private Child child;
    private double lastLocationLatitude;
    private double lastLocationLongitude;
    @Embedded
    private AllowedZone allowedZone = new AllowedZone();

    public Tracking() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public double getLastLocationLatitude() {
        return lastLocationLatitude;
    }

    public void setLastLocationLatitude(double lastLocationLatitude) {
        this.lastLocationLatitude = lastLocationLatitude;
    }

    public double getLastLocationLongitude() {
        return lastLocationLongitude;
    }

    public void setLastLocationLongitude(double lastLocationLongitude) {
        this.lastLocationLongitude = lastLocationLongitude;
    }

    public AllowedZone getAllowedZone() {
        return allowedZone;
    }

    public void setAllowedZone(AllowedZone allowedZone) {
        this.allowedZone = allowedZone;
    }
}