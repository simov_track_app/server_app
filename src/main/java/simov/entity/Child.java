package simov.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
public class Child {
    @Id
    @NotNull
    @Size(min = 4)
    private String name;

    @Column(length = 400)
    private String fcmToken;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "child")
    private Set<Tracking> tracking;

    public Child() {
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Tracking> getTracking() {
        return tracking;
    }

    public void setTracking(Set<Tracking> tracking) {
        this.tracking = tracking;
    }
}