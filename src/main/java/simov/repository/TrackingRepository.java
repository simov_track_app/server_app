package simov.repository;

import org.springframework.data.repository.CrudRepository;
import simov.entity.Child;
import simov.entity.Parent;
import simov.entity.Tracking;

public interface TrackingRepository extends CrudRepository<Tracking, Long>{
    Tracking findByParentAndChild(Parent parent, Child child);
}