package simov.repository;

import org.springframework.data.repository.CrudRepository;
import simov.entity.Child;

public interface ChildRepository extends CrudRepository<Child, Long> {
    Child findByName(String name);
}
