package simov.repository;

import org.springframework.data.repository.CrudRepository;
import simov.entity.Parent;

public interface ParentRepository extends CrudRepository<Parent, Long> {
    Parent findByName(String name);
}