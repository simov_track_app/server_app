package simov.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

@Service
public class NotificationService {
    private static final String FCM_SERVER_TOKEN = "AAAACISQ3AQ:APA91bGoCn_CeGvciVLjzy7AmQCy1bTXy95JOaB1DBnO1qYU0yofBuGWgmg4ipWs0Xp64PgL_l8AlE_jiXkinRxx8mF2n0XGDoMUP0jv8MRTL_qhCgqhugrF7c89xH08jV3HJmjAbOKu";
    private Logger LOG = LoggerFactory.getLogger(NotificationService.class);

    public void postNotification(@NotNull String toToken, @NotNull String title, @NotNull String body) throws IOException {
        LOG.info("Notification will be sent to {}", toToken);

        URL url = new URL("https://fcm.googleapis.com/fcm/send");
        HttpURLConnection fcmSendMessage = (HttpURLConnection) url.openConnection();
        fcmSendMessage.setDoOutput(true);
        fcmSendMessage.setRequestMethod("POST");
        fcmSendMessage.setRequestProperty("Content-Type", "application/json");
        fcmSendMessage.setRequestProperty("Authorization", "key=" + FCM_SERVER_TOKEN);

        String requestBodyData = "{" +
                "   \"notification\": {\n" +
                "      \"title\": \"" + title + "\",\n" +
                "      \"body\": \"" + body + "\"\n" +
                "   },\n" +
                "   \"data\": {\n" +
                "   },\n" +
                "   \"to\": \"" + toToken + "\",\n" +
                "   \"priority\": \"high\"\n" +
                "}";
        fcmSendMessage.getOutputStream().write(requestBodyData.getBytes("UTF-8"));
        fcmSendMessage.getInputStream();
    }

    public void postData(@NotNull String toToken, @NotNull Map<String, String> dataTable) throws IOException {
        LOG.info("Notification will be sent to {}", toToken);

        URL url = new URL("https://fcm.googleapis.com/fcm/send");
        HttpURLConnection fcmSendMessage = (HttpURLConnection) url.openConnection();
        fcmSendMessage.setDoOutput(true);
        fcmSendMessage.setRequestMethod("POST");
        fcmSendMessage.setRequestProperty("Content-Type", "application/json");
        fcmSendMessage.setRequestProperty("Authorization", "key=" + FCM_SERVER_TOKEN);

        ObjectMapper mapper = new ObjectMapper();
        String dataPayload = mapper.writeValueAsString(dataTable);
        LOG.info("Data payload: {}", dataPayload);

        String requestBodyData = "{" +
                "   \"to\": \"" + toToken + "\",\n" +
                "   \"priority\": \"high\",\n" +
                "   \"data\": " + dataPayload +
                "}";
        fcmSendMessage.getOutputStream().write(requestBodyData.getBytes("UTF-8"));
        fcmSendMessage.getInputStream();
    }
}
